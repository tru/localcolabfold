#!/bin/bash
export CONDA_ROOT=/app/colabfold/conda
eval "$(${CONDA_ROOT}/bin/conda shell.bash hook)"
export COLABFOLDDIR=/app/colabfold
conda activate ${COLABFOLDDIR}/colabfold-conda
export PYTHONPATH=${COLABFOLDDIR}

if [ ! -L alphafold ]; then
echo "making alphafold -> /app/colabfold/alphafold as soft link"
ln -s /app/colabfold/alphafold
fi

if [ $# -ge 1 ]; then
echo running python3.7 "$@" from conda  ${COLABFOLDDIR}/colabfold-conda env
python3.7 "$@"
else
echo "usage: singularity run --nv localcolabfold.sif  runner.py-modified"
fi
